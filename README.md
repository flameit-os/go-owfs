# Go OWFS lib

Golang library for interacting with an OWFS 1-wire server

Still missing major functionality, but you can do a hello world directory listing with:

## Authors

Original code was written by [Daniel Hiltgen (dhiltgen)](http://www.hiltgen.com/daniel)

* [Go-OWFS on Github](https://github.com/dhiltgen/go-owfs)

## Example

```go
package main

import (
    "log"

    owfs "gitlab.com/flameit-os/go-owfs"
)

func main() {
    oc, err := owfs.NewClient("localhost:4304")
    if err != nil {
        log.Fatalf("Failed to connect: %s", err)
    }
    sensors, err := oc.Dir("/")
    if err != nil {
        log.Fatalf("Failed to dir: %s", err)
    }
    log.Println("Dir / ", sensors)

    for _, sen := range sensors {

        data, err := oc.Read(sen + "/temperature")
        if err != nil {
            log.Fatalf("Failed to read: %s", err)
        }
        log.Println("Read: "+sen, string(data))
    }

}

```

And you might see something like this:

```
2019/06/06 17:42:20 Dir /  [/28.FF8CAEC01604 /28.FF7E60B41605 /28.FFA0C4B41603 /28.FF451DB41605 /28.FF4DD9C01604 /28.FF5772C01604]
2019/06/06 17:42:20 read /28.FF8CAEC01604 34.3125
2019/06/06 17:42:20 read /28.FF7E60B41605 33.5625
2019/06/06 17:42:20 read /28.FFA0C4B41603 36.5625
2019/06/06 17:42:20 read /28.FF451DB41605 41
2019/06/06 17:42:20 read /28.FF4DD9C01604 26.3125
2019/06/06 17:42:20 read /28.FF5772C01604 26.5625
```
