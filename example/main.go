package main

import (
	"log"

	owfs "gitlab.com/flameit-os/go-owfs"
)

func main() {
	oc, err := owfs.NewClient("localhost:4304")
	if err != nil {
		log.Fatalf("Failed to connect: %s", err)
	}
	sensors, err := oc.Dir("/")
	if err != nil {
		log.Fatalf("Failed to dir: %s", err)
	}
	log.Println("Dir / ", sensors)

	for _, sen := range sensors {

		data, err := oc.Read(sen + "/temperature")
		if err != nil {
			log.Fatalf("Failed to read: %s", err)
		}
		log.Println("Read: "+sen, string(data))
	}

}
